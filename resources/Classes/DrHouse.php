<?php

namespace App\Resources\Classes;

use App\POO\Ex03\House;

class DrHouse extends House
{
    public function diagnose()
    {
        echo "It's not lupus !\n";
    }
}
