<?php

namespace App\POO\Ex03;

abstract class House
{
    abstract public function getHouseName();

    abstract public function getHouseMotto();

    abstract public function getHouseSeat();

    public function introduce()
    {
        echo "House {$this->getHouseName()} of {$this->getHouseSeat()} : \"{$this->getHouseMotto()}\"" . "\n";
    }
}

// -----  CLASSE ABSTRAITE  -----

// Une classe abstraite est une classe qui ne va pas pouvoir être instanciée directement, c’est-à-dire qu’on ne va pas pouvoir manipuler directement.

// Dès qu’une classe possède une méthode abstraite, il va falloir la déclarer comme classes abstraite.

// Lors de l’héritage d’une classe abstraite, les méthodes déclarées comme abstraites dans la classe parent doivent obligatoirement être définies dans la classe enfant avec des signatures (nom et paramètres) correspondantes.

// En effet, en créant un plan « protégé » (puisqu’une classe abstraite ne peut pas être instanciée directement) on force les développeurs à étendre cette classe et on les force également à définir les méthodes abstraites.
