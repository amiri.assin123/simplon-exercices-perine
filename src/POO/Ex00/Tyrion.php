<?php

namespace App\POO\Ex00;

use App\Resources\Classes\Lannister\Lannister;
use ReflectionClass;

class Tyrion extends Lannister
{
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";
    public const SIZE = 'Short';

    public const OH_YES = "Let's do this.\n";
    public const OH_NO = "Not even if I'm drunk !\n";
    public const WHY_NOT = "With pleasure, but only in a tower in Winterfell, then.\n";

    protected function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT;
            echo self::BIRTH_ANNOUNCEMENT;
        }
    }

    public function sleepWith($someone): void
    {
        $reflect = new ReflectionClass($someone);
        $className = $reflect->getShortName();
        switch ($className) {
            case 'Jaime':
                $result = self::OH_NO;
                break;
            case 'Sansa':
                $result = self::OH_YES;
                break;
            case 'Cersei':
                $result = self::OH_NO;
                break;
            default:
                $result = self::OH_NO;
                break;
        }
        echo $result;
    }
}
