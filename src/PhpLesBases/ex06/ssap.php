<?php

$newtab = [];
foreach (array_slice($argv, 1) as $value) {
    $value = preg_replace("/\s+/", ' ', trim($value));
    if (str_word_count($value) > 1) {
        $array = explode(' ', $value);
        $newtab = array_merge($newtab, $array);
    } else {
        $newtab[] = $value;
    }
}

sort($newtab);
foreach ($newtab as $value) {
    echo "$value\n";
}
